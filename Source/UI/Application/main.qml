import QtQuick 2.12
import QtQuick.Controls 2.5

ApplicationWindow {
    visible: true
    width: 920
    height: 720
    minimumWidth: 520
    minimumHeight: 320
    title: qsTr("Thesis")

    ScrollView {
        anchors.fill: parent

        ListView {
            width: parent.width
            model: 120
            delegate: ItemDelegate {
                text: "Test " + (index + 1)
                width: parent.width
            }
        }
    }

    menuBar: MenuBar {
        Menu {
            title: qsTr("&File")
            Action { text: qsTr("&New...") }
            Action { text: qsTr("&Open...") }
            Action { text: qsTr("&Save") }
            Action { text: qsTr("Save &As...") }
            MenuSeparator { }
            Action { text: qsTr("&Quit") }
        }
        Menu {
            title: qsTr("&Edit")
            Action { text: qsTr("Cu&t") }
            Action { text: qsTr("&Copy") }
            Action { text: qsTr("&Paste") }
            MenuSeparator { }
            Menu {
                title: "Find/Replace"
                Action { text: "Find Next" }
                Action { text: "Find Previous" }
                Action { text: "Replace" }
            }
        }
        Menu {
            title: qsTr("&View")
            Action { text: qsTr("&Verilog") }
            Action { text: qsTr("&Faults") }
            Action { text: qsTr("&Memory map") }
        }
        Menu {
            title: qsTr("&Help")
            Action { text: qsTr("&Help") }
            Action { text: qsTr("&Contact &Support...") }
            MenuSeparator { }
            Menu {
                title: "Submit a Bug Report..."
                Action { text: "Please" }
                Action { text: "do" }
                Action { text: "not" }
                Action { text: "do" }
                Action { text: "this :)" }
            }
            MenuSeparator { }
            Action { text: qsTr("Submit &Feedback...") }
            Action { text: qsTr("&Register...") }
            Action { text: qsTr("&Check for Updates...") }
            Action { text: qsTr("&About") }
        }
    }
}
