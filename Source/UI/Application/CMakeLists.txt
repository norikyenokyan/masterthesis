project(Application)

# The main program

set(CMAKE_AUTOUIC ON)
set(CMAKE_AUTOMOC ON)
set(CMAKE_AUTORCC ON)

add_executable(Thesis main.cc qml.qrc)

target_link_libraries(Thesis PRIVATE Logger)

target_compile_definitions(Thesis PRIVATE $<$<OR:$<CONFIG:Debug>,$<CONFIG:RelWithDebInfo>>:QT_QML_DEBUG>)

target_link_libraries(Thesis PRIVATE
        Qt5::Core
        Qt5::Quick
        Qt5::Gui
        Qt5::Widgets)
